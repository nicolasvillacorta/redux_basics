import * as actionTypes from '../actions';

const initialState = {
    counter: 0,
    results: []
}
// SIEMPRE HAY QUE USAR COPIAS DEL ESTADO ORIGINAL, NUNCA CAMBIARLO, DEBE SER INMUTABLE.
// Los values +5 y -5 generalmente no se hardcodean, porque suelen recibirse del componente.
// Generalmente no se manda un value, se manda un objeto de JS llamado payload con todos los valores dentro.
const reducer = (state = initialState, action) => {
    // El switch no necesita los break, porque tiene un return.
    switch(action.type){
        case actionTypes.STORE_RESULT:
            return  {
                ...state,
                results: state.results.concat({id: new Date(), value: action.result}) // Es como push, pero push manipula el valor original, este devuelve un array nuevo con 1 valor mas.
            }
        case actionTypes.DELETE_RESULT:
            // const id = 2;
            // const newArray = [...state.results];
            // newArray.results.splice(id, 1); [ESTA OPCION NO ESTARIA MAL, PORQUE HICE UNA COPIA DEL ARRAY Y LA PONGO EN EL STATE, PERO EN ESTE EJEMPLO VOY A USAR EL FILTER]
            /// state.results.splice(id, 1) - [ESTO ESTA MAL PORQUE MUTA EL ESTADO]
            const updatedArray = state.results.filter(result => result.id !== action.resultElId); // Filter retorna un array nuevo, no toca el original.
            return {
                ...state,
                results: updatedArray
            }
    }
    return state; // Este return tiene que estar, ya que si este reducer no cumple con ningun switch, tiene que retornar para que no rompa la app.
}

export default reducer;