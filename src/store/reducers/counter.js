import * as actionTypes from '../actions';

const initialState = {
    counter: 0,
    results: []
}
// SIEMPRE HAY QUE USAR COPIAS DEL ESTADO ORIGINAL, NUNCA CAMBIARLO, DEBE SER INMUTABLE.
// Los values +5 y -5 generalmente no se hardcodean, porque suelen recibirse del componente.
// Generalmente no se manda un value, se manda un objeto de JS llamado payload con todos los valores dentro.
const reducer = (state = initialState, action) => {
    // El switch no necesita los break, porque tiene un return.
    switch(action.type){
        case actionTypes.INCREMENT:
            return {
                ...state,
                counter: state.counter + 1 
            }
        case actionTypes.DECREMENT: 
            return {
                ...state,
                counter: state.counter -1
            }
        case actionTypes.ADD: 
            return {
                ...state,
                counter: state.counter + action.value
            }
        case actionTypes.REMOVE: 
            return {
                ...state,
                counter: state.counter - action.value
            }
    }
    return state; // Este return tiene que estar, ya que si este reducer no cumple con ningun switch, tiene que retornar para que no rompa la app.
}

export default reducer;