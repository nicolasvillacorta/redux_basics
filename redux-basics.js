/*
    ESTE ARCHIVO ES DE EJEMPLO, NO HACE FALTA TENERLO EN MIS PROYECTOS.
*/

const redux = require('redux');
const createStore = redux.createStore;

const initialState = {
    counter: 0
}

// Reducer [state = initialState, significa que si se recibe un state undefined, lo inicializa.]. No se debe aumentar el estado original, tiene que ser inmutable
//      se tiene que retornar un state nuevo, un nuevo objeto de js.
const rootReducer = (state = initialState, action) => {

    if(action.type === 'INC_COUNTER'){
        return {
            ...state,
            counter: state.counter + 1
        }
    }

    if(action.type === 'ADD_COUNTER'){
        return {
            ...state,
            counter: state.counter + action.value
        }
    }

    return state;
};

// Store
const store = createStore(rootReducer);
console.log(store.getState());

// Subscription
store.subscribe(() => {
    console.log('[Subscription]', store.getState());
});

// Dispatching Action
store.dispatch({type: 'INC_COUNTER'});
store.dispatch({type: 'ADD_COUNTER', value: 10});
console.log(store.getState());


