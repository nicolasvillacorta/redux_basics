## El middleware sirve para logear cosas desde que  se dispatchea una accion hasta que afecta al state, pero su mayor utilidad es para ejecutar codigo asincronicamente.

# App  de ejemplo usando Redux.

Siempre tengo que usar redux?? Depende que tan grande/compleja sea la aplicacion. Si es pequeña, no vale la pena para nada, solo si es muy grande y maneja un estado importante.

	## Los reducer suele crearse en una carpeta a la par que los componentes y los containers, dentro de la carpeta store tengo los reducers.
	## En el index.js importo la store de redux, y el componente Provider de 'react-redux', con este componente wrappeo el componente App. El provider
		recibe como prop la store.

	## En el componente que tenga el estado, importo { connect } y lo uso para exportar el componente, quedando de la siguiente manera, (fuera de la clase del componente)
	
		const mapStateToProps = state => {
			return {
				ctr: state.counter      
			};
		};

		const mapDispatchToProps = dispatch => {
			return {
				onIncrementCounter: () => dispatch({type: 'INCREMENT'})
			};
		};

		export default connect(mapStateToProps, mapDispatchToProps)(Counter);
		- Para terminar, las props que muestro o paso a otros componentes dentro, las accedo desde "this.props.ctr", el nombre que setie en el map, ya no accedo mas desde "this.state.."
		- Las funciones dispatch las termino llamando tambien desde "this.props.incrementOnCount" por ejemplo.
		- Si el component no hace acciones, el segundo argumento en el export no hace falta, y si no tiene state el primero puedo ponerle "null".

    
	## Este error se soluciono subiendo la version de React y ReactDom a 16.3, estaba en 16.0
		"TypeError: __WEBPACK_IMPORTED_MODULE_0_react___default.a.createContext is not a function"